package main

import (
	"fmt"
	"log"
	"strings"
	"text/template"
	"time"

	"codeberg.org/samwhited/shopclock/internal/storage"

	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
)

type punches struct {
	theme     *material.Theme
	list      widget.List
	backBtn   widget.Clickable
	exportBtn widget.Clickable
	items     []storage.Punch
	back      layout.Widget
	export    func(*punches)
	debug     *log.Logger
}

func (w *punches) Layout(gtx layout.Context) layout.Dimensions {
	if w.backBtn.Clicked() && w.back != nil {
		return w.back(gtx)
	}
	if w.exportBtn.Clicked() && w.export != nil {
		w.export(w)
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceBetween,
	}.Layout(
		gtx,
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			title := material.H5(w.theme, fmt.Sprintf("Punches for %s", time.Now().Format("2006-01-02")))
			title.Alignment = text.Middle
			return title.Layout(gtx)
		}),
		layout.Rigid(layout.Spacer{Height: unit.Dp(25)}.Layout),
		layout.Flexed(1, func(gtx layout.Context) layout.Dimensions {
			return material.List(w.theme, &w.list).Layout(gtx, len(w.items), func(gtx layout.Context, idx int) layout.Dimensions {
				item := w.items[idx]
				var buf strings.Builder
				err := template.Must(template.New("list").Funcs(map[string]interface{}{
					"tfmt": func(t time.Time) string {
						return t.Format("15:04")
					},
				}).Parse(`
{{- .Name }}{{ if .Email }} <{{ .Email }}>{{ end }} ({{ .Reason }}) at {{ .Time | tfmt }}`)).Execute(&buf, item)
				if err != nil {
					w.debug.Printf("error executing template: %v", err)
				}
				return material.Body1(w.theme, buf.String()).Layout(gtx)
			})
		}),
		layout.Rigid(layout.Spacer{Height: unit.Dp(25)}.Layout),
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			return layout.Flex{
				Spacing: layout.SpaceBetween,
			}.Layout(
				gtx,
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					btn := material.Button(w.theme, &w.backBtn, "Back")
					return btn.Layout(gtx)
				}),
				layout.Rigid(layout.Spacer{Width: unit.Dp(25)}.Layout),
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					btn := material.Button(w.theme, &w.exportBtn, "Export")
					return btn.Layout(gtx)
				}),
			)
		}),
	)
}
