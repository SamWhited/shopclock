package storage

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "modernc.org/sqlite"
)

const (
	dbDriver = "sqlite"
)

// Punch represents a single punch in.
type Punch struct {
	Name       string
	Email      string
	Reason     string
	Newsletter bool
	Time       time.Time
}

// DB is a database with several pre-prepared queries.
type DB struct {
	*sql.DB

	punchIn *sql.Stmt
	today   *sql.Stmt
	debug   *log.Logger
}

// Today returns a list of punches that happened today.
func (db *DB) Today(ctx context.Context) ([]Punch, error) {
	rows, err := db.today.QueryContext(ctx)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := rows.Close()
		if err != nil {
			db.debug.Printf("error closing rows: %v", err)
		}
	}()
	var list []Punch
	for rows.Next() {
		p := Punch{}
		var epoch int64
		err := rows.Scan(&p.Name, &p.Email, &p.Newsletter, &p.Reason, &epoch)
		if err != nil {
			return nil, err
		}
		p.Time = time.Unix(epoch, 0)
		list = append(list, p)
	}
	return list, rows.Err()
}

// PunchIn adds a row to the punches.
// It ignores the Time field and uses the current time.
func (db *DB) PunchIn(ctx context.Context, p Punch) error {
	_, err := db.punchIn.ExecContext(ctx, p.Name, p.Email, p.Newsletter, p.Reason)
	return err
}

// New opens the database and pre-prepares a number of queries.
func New(ctx context.Context, dbFile, schema string, debug *log.Logger) (*DB, error) {
	sqlDB, err := sql.Open(dbDriver, dbFile)
	if err != nil {
		e := sqlDB.Close()
		if e != nil {
			debug.Printf("error closing DB while handling another error: %v", err)
		}
		return nil, fmt.Errorf("error opening DB: %w", err)
	}
	_, err = sqlDB.Exec("PRAGMA foreign_keys = ON")
	if err != nil {
		e := sqlDB.Close()
		if e != nil {
			debug.Printf("error closing DB while handling foreign keys error: %v", err)
		}
		return nil, fmt.Errorf("error enabling foreign keys: %w", err)
	}
	db := &DB{
		DB:    sqlDB,
		debug: debug,
	}
	_, err = db.Exec(schema)
	if err != nil {
		e := sqlDB.Close()
		if e != nil {
			debug.Printf("error closing DB while handling schema error: %v", err)
		}
		return nil, fmt.Errorf("error applying schema: %w", err)
	}
	db.punchIn, err = db.PrepareContext(ctx, `
INSERT INTO punches (name, email, newsletter, reason)
	VALUES ($1, $2, $3, $4)`)
	if err != nil {
		return nil, err
	}
	db.today, err = db.PrepareContext(ctx, `
SELECT name, email, newsletter, reason, punchTime
	FROM punches
	WHERE date(datetime(punchTime, 'unixepoch'), 'localtime') = date('now', 'localtime')
	ORDER BY punchTime ASC`)
	if err != nil {
		return nil, err
	}
	return db, nil
}
