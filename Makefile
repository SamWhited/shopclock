GOFILES!=find . -name '*.go'
GO=go
TAGS=

shopclock: go.mod go.sum $(GOFILES)
	$(GO) build \
		-trimpath \
		-tags "$(TAGS)" \
		-o $@ \

shopclock.apk: go.mod go.sum $(GOFILES)
	gogio -target android .
	adb -t 5 install $@

shopclock.aar: go.mod go.sum $(GOFILES)
	gogio -o $@ -target android -buildmode archive blog.samwhited.com/shopclock
