-- Things to note if you're not familiar with sqlite3:
--
--  - After connecting always run "PRAGMA foreign_keys = ON" or foreign key
--    constraints will not be honored.
--  - Primary keys may be NULL (and thus always need a NOT NULL constraint)
--  - Tables with primary keys that are not integers and that don't need auto
--    incrementing counters can use WITHOUT ROWID to save some space.

CREATE TABLE IF NOT EXISTS punches (
	id         INTEGER PRIMARY KEY NOT NULL,
	name       TEXT    NOT NULL,
	email      TEXT,
	newsletter BOOLEAN NOT NULL,
	reason     TEXT    NOT NULL,
	punchTime  INTEGER NOT NULL DEFAULT (CAST(strftime('%s', 'now') AS INTEGER))
);
