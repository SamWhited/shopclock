// The shopclock command lets users clock in for a shop shift.
package main

import (
	"context"
	_ "embed"
	"encoding/csv"
	"flag"
	"image/color"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"codeberg.org/samwhited/shopclock/internal/storage"

	"gioui.org/app"
	"gioui.org/font/gofont"
	"gioui.org/io/key"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
)

const (
	appName = "Shop Clock"
)

//go:embed schema.sql
var schema string

func main() {
	// Setup logging
	logger := log.New(os.Stderr, "", log.LstdFlags)
	debug := log.New(io.Discard, "DEBUG ", log.LstdFlags)

	// Setup flags
	var (
		verbose bool
		dbFile  = "shopclock.db"
	)
	flags := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flags.BoolVar(&verbose, "v", verbose, "enable verbose debug logging")
	flags.StringVar(&dbFile, "db", dbFile, "the database file")
	err := flags.Parse(os.Args[1:])
	switch err {
	case flag.ErrHelp:
		return
	case nil:
	default:
		logger.Fatalf("error parsing flags: %v", err)
	}

	// Configure logging
	if verbose {
		debug.SetOutput(os.Stderr)
	}

	// Setup the database
	db, err := storage.New(context.Background(), dbFile, schema, debug)
	if err != nil {
		logger.Fatalf("error initializing storage: %v", err)
	}

	// Setup the main UI window
	go func() {
		w := app.NewWindow(
			app.Title(appName),
			app.Size(unit.Dp(400), unit.Dp(600)),
			app.MinSize(unit.Dp(400), unit.Dp(600)),
		)
		err := run(w, db, logger, debug)
		if err != nil {
			logger.Fatal(err)
		}
		os.Exit(0)
	}()
	app.Main()
}

func run(w *app.Window, db *storage.DB, logger, debug *log.Logger) error {
	th := material.NewTheme(gofont.Collection())
	var ops op.Ops
	maroon := color.NRGBA{R: 127, G: 0, B: 0, A: 255}
	clockIn := &punchInPage{
		name: widget.Editor{
			SingleLine: true,
			Submit:     true,
		},
		email: widget.Editor{
			SingleLine: true,
			Submit:     true,
			InputHint:  key.HintEmail,
		},
		theme:          th,
		reasonRequired: widget.Border{Color: maroon, Width: unit.Dp(2)},
		nameRequired:   widget.Border{Color: maroon, Width: unit.Dp(2)},
		submit: func(w *punchInPage) {
			err := db.PunchIn(context.Background(), storage.Punch{
				Name:       w.Name(),
				Email:      w.Email(),
				Reason:     w.Reason(),
				Newsletter: w.Newsletter(),
			})
			if err != nil {
				logger.Printf("error submitting punch: %v", err)
			}
		},
	}
	punchList := &punches{
		theme: th,
		list: widget.List{
			List: layout.List{
				Axis: layout.Vertical,
			},
		},
		export: func(p *punches) {
			fd, err := os.Create(time.Now().Format("2006-01-02.csv"))
			if err != nil {
				logger.Printf("error creating export file: %v", err)
			}
			defer func() {
				err := fd.Close()
				if err != nil {
					debug.Printf("error closing export file: %v", err)
				}
			}()
			w := csv.NewWriter(fd)
			for _, punch := range p.items {
				err := w.Write([]string{
					punch.Name,
					punch.Email,
					punch.Reason,
					strconv.FormatBool(punch.Newsletter),
					punch.Time.Format(time.RFC3339),
				})
				if err != nil {
					logger.Printf("error writing to export file: %v", err)
					return
				}
			}
			w.Flush()
			err = w.Error()
			if err != nil {
				logger.Printf("error flushing export file: %v", err)
			}
		},
		debug: debug,
	}
	p := &pages{
		widgets: []layout.Widget{
			clockIn.Layout,
			punchList.Layout,
		},
	}
	punchList.back = func(gtx layout.Context) layout.Dimensions {
		p.top = 0
		return p.Layout(gtx)
	}
	clockIn.list = func(gtx layout.Context) layout.Dimensions {
		punches, err := db.Today(context.Background())
		if err != nil {
			logger.Printf("error fetching punches: %v", err)
		} else {
			punchList.items = punches
			p.top = 1
		}
		return p.Layout(gtx)
	}
	for e := range w.Events() {
		switch e := e.(type) {
		case system.DestroyEvent:
			return e.Err
		case system.FrameEvent:
			gtx := layout.NewContext(&ops, e)
			p.Layout(gtx)
			e.Frame(gtx.Ops)
		}
	}
	return nil
}
