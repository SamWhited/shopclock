package main

import (
	"gioui.org/layout"
	"gioui.org/unit"
)

type pages struct {
	widgets []layout.Widget
	top     int
}

func (p *pages) Layout(gtx layout.Context) layout.Dimensions {
	return layout.Inset{
		Top:    unit.Dp(25),
		Bottom: unit.Dp(25),
		Right:  unit.Dp(35),
		Left:   unit.Dp(35),
	}.Layout(gtx, p.widgets[p.top])
}
