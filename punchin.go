package main

import (
	"gioui.org/layout"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
)

type punchInPage struct {
	name           widget.Editor
	email          widget.Editor
	consent        widget.Bool
	reasons        widget.Enum
	reasonRequired widget.Border
	nameRequired   widget.Border
	punchIn        widget.Clickable
	clicked        bool
	showPunches    widget.Clickable
	submit         func(*punchInPage)
	list           layout.Widget
	theme          *material.Theme
}

// Newsletter returns whether the user wants to sign up for the newsletter or
// not.
func (w *punchInPage) Newsletter() bool {
	return w.consent.Pressed()
}

// Name returns the name of the person punching in.
func (w *punchInPage) Name() string {
	return w.name.Text()
}

// Email returns the email of the person punching in.
func (w *punchInPage) Email() string {
	return w.email.Text()
}

// Reason returns the reason the person is checking in.
func (w *punchInPage) Reason() string {
	return w.reasons.Value
}

// Clears clears the form, resetting it to its initial state.
func (w *punchInPage) Clear() {
	w.clicked = false
	w.email.SetText("")
	w.name.SetText("")
	w.consent.Value = false
	w.reasons.Value = ""
}

func (w *punchInPage) Layout(gtx layout.Context) layout.Dimensions {
	if w.showPunches.Clicked() && w.list != nil {
		return w.list(gtx)
	}

	// Storage that we clicked the submit button at some time in the past so that
	// we can draw the "this is a required field" indicator if the submission
	// doesn't work.
	if !w.clicked {
		w.clicked = w.punchIn.Clicked()
	}
	required := w.Reason() == "" || w.Name() == ""
	// If we clicked the button on this redraw (not at some point in the past),
	// nothing is currently required, and we actually have an action to perform
	// when submitting the form, do so.
	if w.submit != nil && w.punchIn.Clicked() && !required {
		w.submit(w)
		w.Clear()
	}

	return layout.Flex{
		Axis:    layout.Vertical,
		Spacing: layout.SpaceBetween,
	}.Layout(
		gtx,
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			title := material.H2(w.theme, appName)
			title.Alignment = text.Middle
			return title.Layout(gtx)
		}),
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			name := layout.Flex{Spacing: layout.SpaceSides}.Layout(
				gtx,
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					return layout.Flex{
						Axis:    layout.Vertical,
						Spacing: layout.SpaceSides,
					}.Layout(gtx,
						layout.Rigid(func(gtx layout.Context) layout.Dimensions {
							label := material.Label(w.theme, unit.Sp(20), "Name:")
							return label.Layout(gtx)
						}),
						layout.Rigid(layout.Spacer{Height: unit.Dp(25)}.Layout),
						layout.Rigid(func(gtx layout.Context) layout.Dimensions {
							label := material.Label(w.theme, unit.Sp(20), "Email:")
							return label.Layout(gtx)
						}),
					)
				}),
				layout.Rigid(layout.Spacer{Width: unit.Dp(25)}.Layout),
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					return layout.Flex{
						Axis:    layout.Vertical,
						Spacing: layout.SpaceSides,
					}.Layout(gtx,
						layout.Rigid(func(gtx layout.Context) layout.Dimensions {
							name := material.Editor(w.theme, &w.name, "First Last")
							name.TextSize = unit.Sp(20)
							return name.Layout(gtx)
						}),
						layout.Rigid(layout.Spacer{Height: unit.Dp(25)}.Layout),
						layout.Rigid(func(gtx layout.Context) layout.Dimensions {
							name := material.Editor(w.theme, &w.email, "me@example.org")
							name.TextSize = unit.Sp(20)
							return name.Layout(gtx)
						}),
					)
				}),
			)

			if w.clicked && w.Name() == "" {
				return w.nameRequired.Layout(gtx, func(gtx layout.Context) layout.Dimensions {
					return name
				})
			}
			return name
		}),
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			return layout.Flex{
				Spacing: layout.SpaceSides,
			}.Layout(
				gtx,
				layout.Rigid(material.CheckBox(w.theme, &w.consent, "I'd like to receive your newsletter").Layout),
			)
		}),
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			return layout.Flex{
				Axis: layout.Vertical,
			}.Layout(gtx,
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					waiver := material.Body1(w.theme, "What is your primary reason for coming today?")
					waiver.Alignment = text.Middle
					return waiver.Layout(gtx)
				}),
				layout.Rigid(layout.Spacer{Height: unit.Dp(25)}.Layout),
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					reason := layout.Flex{
						Spacing: layout.SpaceSides,
					}.Layout(
						gtx,
						layout.Rigid(material.RadioButton(w.theme, &w.reasons, "volunteer", "Volunteer").Layout),
						layout.Rigid(material.RadioButton(w.theme, &w.reasons, "customer", "Customer").Layout),
						layout.Rigid(material.RadioButton(w.theme, &w.reasons, "donor", "Donate").Layout),
					)
					if w.clicked && w.Reason() == "" {
						return w.reasonRequired.Layout(gtx, func(gtx layout.Context) layout.Dimensions {
							return reason
						})
					}
					return reason
				}),
			)

		}),
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			waiver := material.Body1(w.theme, "By punching in I acknowledge that I have read and accept the waiver of liability.")
			waiver.Alignment = text.Middle
			return waiver.Layout(gtx)
		}),
		layout.Rigid(func(gtx layout.Context) layout.Dimensions {
			return layout.Flex{
				Spacing: layout.SpaceBetween,
				Axis:    layout.Vertical,
			}.Layout(
				gtx,
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					btn := material.Button(w.theme, &w.punchIn, "Punch In")
					return btn.Layout(gtx)
				}),
				layout.Rigid(layout.Spacer{Height: unit.Dp(25)}.Layout),
				layout.Rigid(func(gtx layout.Context) layout.Dimensions {
					btn := material.Button(w.theme, &w.showPunches, "Show Punches")
					return btn.Layout(gtx)
				}),
			)
		}),
	)
}
